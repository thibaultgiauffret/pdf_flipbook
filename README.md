# PDF Flipbook

"PDF Flipbook" est une application web permettant de visualiser des fichiers PDF sous forme de livre numérique.

Une version en ligne de "PDF Flipbook" est accessible [ici](https://www.ensciences.fr/addons/pdf_flipbook/) ou [là](https://thibaultgiauffret.forge.apps.education.fr/pdf_flipbook/).

**🚧 ATTENTION :** Ce projet est en cours de développement.


## 🖥 Tester en local

- Installer NodeJS et NPM
- Installer git : [https://git-scm.com/downloads](https://git-scm.com/downloads)
- Cloner le dépôt : `git clone https://forge.apps.education.fr/thibaultgiauffret/pdf_flipbook.git`
- Installer les dépendances : `npm install`
- Lancer le serveur local de développement : `npm run dev`

## 🛠 Contribuer

Vous pouvez contribuer au projet en proposant des améliorations ou des corrections de bugs [juste là](https://forge.apps.education.fr/thibaultgiauffret/pdf_flipbook/-/issues) ou en proposant des requêtes de fusion [ici](https://forge.apps.education.fr/thibaultgiauffret/pdf_flipbook/-/merge_requests).

## ⚖ Crédits et licence

"PDF Flipbook" utilise les bibliothèques open-source suivantes :
- [react-pageflip](https://www.npmjs.com/package/react-pageflip) (MIT)
- [react-pdf](https://www.npmjs.com/package/react-pdf) (MIT)
- [pdf.js](https://mozilla.github.io/pdf.js/) (Apache 2.0)
- [react-bootstrap](https://www.npmjs.com/package/react-bootstrap) (MIT)
- [fontawesome](https://fontawesome.com/) (CC BY 4.0)

"PDF Flipbook" est sous licence GNU GPL v3. Vous pouvez consulter le texte complet de la licence [ici](https://www.gnu.org/licenses/gpl-3.0.html).
