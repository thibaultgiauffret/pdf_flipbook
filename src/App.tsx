// Components
import { Alert, Button, Container, Card, Form, InputGroup } from 'react-bootstrap';
import HTMLFlipBook from "react-pageflip"
import { Document, Page, pdfjs } from 'react-pdf';

pdfjs.GlobalWorkerOptions.workerSrc = new URL(
    'pdfjs-dist/build/pdf.worker.min.mjs',
    import.meta.url,
).toString();
import 'react-pdf/dist/Page/TextLayer.css';
import 'react-pdf/dist/Page/AnnotationLayer.css';

// CSS
import styles from './App.module.css';

// Functions
import { useState, useEffect, useRef } from 'react';

// Icons
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
library.add(fas)
library.add(fab)

function App() {

    // File data type
    type FileData = {
        status: string,
        file: string,
    }

    interface FlipBook {
        pageFlip: () => {
            flipPrev: () => void;
            flipNext: () => void;
        };
    }

    const flipBookRef = useRef<FlipBook>(null);

    // File data and status states
    const [fileData, setFileData] = useState<FileData>({
        status: '',
        file: ''
    });
    const [numPages, setNumPages] = useState<number>(0);
    const [pageNumber, setPageNumber] = useState<number>(1);
    const [rerenderKey, setRerenderKey] = useState(0);

    // Get current view width and height and update the state on resize
    const [viewWidth, setViewWidth] = useState(document.getElementById('pageFlipView')?.clientWidth || document.documentElement.clientWidth);
    const [viewHeight, setViewHeight] = useState(document.getElementById('pageFlipView')?.clientHeight || document.documentElement.clientHeight);
    const [padding, setPadding] = useState(0);

    useEffect(() => {
        // Listen for window resize events
        window.addEventListener('resize', updateViewSize);
        // Update the view size on first load
        updateViewSize();
        return () => {
            window.removeEventListener('resize', updateViewSize);
        };
    }, []);

    const updateViewSize = () => {
        // Get the maximum width and height for the view
        let maxHeight = window.innerHeight * 0.9;
        let maxWidth = (maxHeight * Math.sqrt(2));

        // If the width is greater than the window width, fit the width
        if (maxWidth > window.innerWidth) {
            maxWidth = window.innerWidth;
            maxHeight = (maxWidth / Math.sqrt(2));
        }

        const padding = (window.innerHeight - maxHeight) / 2;

        // Update the view size
        setViewWidth(maxWidth);
        setViewHeight(maxHeight);
        setPadding(padding);

        // Update the rerender key to force the pageflip component to rerender
        setRerenderKey(rerenderKey + 1);
    };

    // Handle page number
    function onDocumentLoadSuccess({ numPages }: { numPages: number }): void {
        setNumPages(numPages);
    }

    const pages = [];
    for (let i = 1; i <= numPages; i++) {
        pages.push(
            <div key={i}>
                <Page pageNumber={i} height={viewHeight} />
            </div>
        );
    }

    return (
        <>
            {/* File input */}
            <Container id="configContainer"
                className='p-4'>
                <h1 className={styles.title}>
                    <img src="./logo.svg" alt="logo" className="me-2" width="45px" height="45px" />
                    <span>PDF Flipbook</span>
                </h1>
                <Form.Label>Importer un fichier PDF</Form.Label>
                <InputGroup>
                    <Form.Control
                        id="fileInput"
                        type="file" accept="application/pdf"
                        onChange={(e) => {
                            const file = (e.target as HTMLInputElement).files?.[0];
                            if (file) {
                                readPDFFile(URL.createObjectURL(file)).then((fileContent) => {
                                    // Set the file data
                                    setFileData({
                                        status: 'success',
                                        file: fileContent
                                    });
                                    // Hide the config container
                                    showHideFlipBook(true);
                                }).catch((error) => {
                                    // Set the file data
                                    setFileData({
                                        status: error,
                                        file: '',
                                    });
                                });

                            }
                        }} />
                </InputGroup>
                <Form.Label className='mt-3'>Ou saisir un lien vers un fichier PDF</Form.Label>
                <InputGroup>
                    <Form.Control
                        type="text"
                        placeholder={
                            window.location.href.split('/').slice(0, -1).join('/') + '/test.pdf'
                        }
                        value={window.location.href.split('/').slice(0, -1).join('/') + '/test.pdf'}
                        id="urlInput" />
                    <Button variant="success" onClick={() => {
                        // Get the url input value
                        const urlInput = document.getElementById('urlInput') as HTMLInputElement;
                        const file = urlInput.value;
                        // Open the file
                        readPDFFile(file).then((fileContent) => {
                            // Set the file data
                            setFileData({
                                status: 'success',
                                file: fileContent
                            });

                            showHideFlipBook(true);
                        }).catch((error) => {
                            // Set the file data
                            setFileData({
                                status: error,
                                file: '',
                            });
                        });
                    }}>
                        <FontAwesomeIcon icon="arrow-right" />
                    </Button>
                </InputGroup>
                <Form.Text className="text-muted">
                    Assurez-vous que le fichier est bien accessible publiquement (CORS).
                </Form.Text>
                {/* Credits at the bottom */}
                <div className={styles.loaderCredits}>
                    <img src="./ensciences.svg" alt="Logo" className={styles.loaderLogo} /><br />
                    <span className={styles.loaderBrand}>EnSciences<br />
                        <span className={styles.loaderDate}>Th. G &copy; 2024</span>
                    </span>
                </div>
            </Container>
            {/* Display pdf is ext is pdf */}
            {
                fileData.status === 'success' ?
                    <div id="pageFlipViewWrapper" className={styles.pageFlipViewWrapper}>
                        {/* Toggle configContainer */}
                        <Button variant="primary" onClick={() => {
                            showHideFlipBook(false);
                        }} className={styles.toggleConfigContainerBtn} id="toggleConfigContainerBtn">
                            <FontAwesomeIcon icon="home" />
                        </Button>
                        <div id="pageFlipView" className={styles.pageFlipView} style={{
                            width: viewWidth,
                            height: viewHeight,
                            paddingTop: padding,
                            paddingBottom: padding,
                        }} key={rerenderKey}>
                            <Document file={fileData.file} onLoadSuccess={onDocumentLoadSuccess}>
                                <HTMLFlipBook
                                    width={viewWidth}
                                    height={viewWidth * Math.sqrt(2)}
                                    size='stretch'
                                    maxShadowOpacity={0.5}
                                    showCover={true}
                                    mobileScrollSupport={true}
                                    onFlip={(e) => {
                                        setPageNumber(e.data + 1);
                                    }}
                                    style={{}}
                                    className={''}
                                    startPage={0}
                                    minWidth={0}
                                    maxWidth={0}
                                    minHeight={0}
                                    maxHeight={0}
                                    drawShadow={true}
                                    flippingTime={1000}
                                    usePortrait={false}
                                    startZIndex={0}
                                    autoSize={true}
                                    clickEventForward={false}
                                    useMouseEvents={true}
                                    swipeDistance={0}
                                    showPageCorners={true}
                                    disableFlipByClick={false}
                                    ref={flipBookRef}>
                                    {pages}
                                </HTMLFlipBook>
                            </Document>
                        </div>
                        {/* Page controls */}
                        <div className={styles.pageControls}>
                            <Card>
                                <Card.Body className='p-1'>
                                    <Button variant="secondary" size="sm" onClick={() => {
                                        if (pageNumber > 1) {
                                            setPageNumber(pageNumber - 1);
                                            flipBookRef.current?.pageFlip().flipPrev();
                                        }
                                    }}><FontAwesomeIcon icon="arrow-left" /></Button>
                                    <span className="mx-3">{pageNumber}/{numPages}</span>
                                    <Button variant="secondary" size="sm" onClick={() => {
                                        if (pageNumber < numPages) {
                                            setPageNumber(pageNumber + 1);
                                            flipBookRef.current?.pageFlip().flipNext();
                                        }
                                    }}><FontAwesomeIcon icon="arrow-right" /></Button>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                    :
                    <Container className='text-center'>
                        {fileData.status === 'not_pdf' ?
                            <Alert variant="danger">
                                <FontAwesomeIcon icon="exclamation-triangle" />&nbsp;Le fichier n'est pas au format pdf...
                            </Alert>
                            :
                            fileData.status === 'read_error' ?
                                <Alert variant="danger">
                                    <FontAwesomeIcon icon="exclamation-triangle" />&nbsp;Une erreur est survenue lors de la lecture du fichier !
                                </Alert>
                                :
                                fileData.status === 'not_found' &&
                                <Alert variant="danger">
                                    <FontAwesomeIcon icon="exclamation-triangle" />&nbsp;Le fichier n'a pas été trouvé...
                                </Alert>
                        }
                    </Container>
            }
        </>
    );
}

// Read file function
function readPDFFile(file: string): Promise<string> {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', file, true);
        xhr.responseType = 'blob';

        xhr.onload = () => {
            if (xhr.status === 200) {
                const blob = xhr.response;
                // Check if the file is a pdf
                if (blob.type === 'text/html') {
                    reject('not_found');
                } else if (blob.type !== 'application/pdf') {
                    reject('not_pdf');
                }
                console.log(blob.type);
                const reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = () => {
                    resolve(reader.result as string);
                };
            } else {
                reject('read_error');
            }
        };

        xhr.onerror = () => reject(xhr.statusText);

        xhr.send();
    });
}

// Show/hide flipbook
function showHideFlipBook(show: boolean) {
    const configContainer = document.getElementById('configContainer');
    const pageFlipViewWrapper = document.getElementById('pageFlipViewWrapper');
    if (show) {
        // Hide the file input
        if (configContainer) {
            configContainer.style.display = 'none';
        }
        // Show the page flip view
        if (pageFlipViewWrapper) {
            pageFlipViewWrapper.style.display = 'block';
        }
    } else {
        // Show the file input
        if (configContainer) {
            configContainer.style.display = 'block';
        }
        // Hide the page flip view
        if (pageFlipViewWrapper) {
            pageFlipViewWrapper.style.display = 'none';
        }
    }
}

export default App;